<?php

namespace App\Http\Controllers;

use App\Repositories\PageRepository as Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PagesController extends Controller
{
    /**
     * @var Page
     */
    private $page;

    /**
     * PagesController constructor.
     * @param Page $page
     */
    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    /**
     * Show page add form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAddForm()
    {
        return view('index');
    }

    /**
     * Save page to storage and to DB
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'url' => 'required|url|max:255'
        ]);

        $this->page->savePage($request->only('url'));

        return back()->with('success', 'Страница успешно сохранена');
    }

    /**
     * Show all pages
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function all()
    {
        $pages = $this->page->all();

        return view('pages', compact('pages'));
    }

    /**
     * Show html of a page
     *
     * @param $hash
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function show($hash)
    {
        $page = $this->page->findBy('hash', $hash);
        $html = Storage::disk('pages')->get($page->hash.'.txt');

        return $html;
    }


}
