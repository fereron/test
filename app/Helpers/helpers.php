<?php

/**
 * Create hash
 *
 * @return string
 */
function createHash() :string
{
    $hash = array(
        'time_low' => 0,
        'time_mid' => 0,
        'time_hi' => 0,
        'clock_seq_hi' => 0,
        'clock_seq_low' => 0,
    );

    $hash['time_low'] = mt_rand(0, 0xffff) + (mt_rand(0, 0xffff) << 16);
    $hash['time_mid'] = mt_rand(0, 0xffff);
    $hash['time_hi'] = (4 << 12) | (mt_rand(0, 0x1000));
    $hash['clock_seq_hi'] = (1 << 7) | (mt_rand(0, 128));
    $hash['clock_seq_low'] = mt_rand(0, 255);

    $hash = sprintf('%08x',
        $hash['time_low'],
        $hash['time_mid'],
        $hash['time_hi'],
        $hash['clock_seq_hi'],
        $hash['clock_seq_low']
    );

    return $hash;
}

/**
 * Change relative url to absolute
 *
 * @param $html
 * @param $domain
 * @return string
 */
function uriToAbsolute($html, $domain) :string
{
    if (mb_substr($domain, -1) == '/') {
        $domain = mb_substr($domain, 0, -1);
    }

    $pattern = [
        '/href\s*=\s*\"\s*(?!http:\/\/|https:\/\/|ftp:\/\/|mailto:)/',
        '/src\s*=\s*\"\s*(?!http:\/\/|https:\/\/|ftp:\/\/)/'
    ];

    $replacement = [
        'href="'.$domain,
        'src="'.$domain
    ];

    return preg_replace($pattern, $replacement, $html);
}