<?php

namespace App\Repositories;

use App\Page;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;

class PageRepository extends Repository
{
    /**
     * @return mixed
     */
    function model()
    {
        return Page::class;
    }

    /**
     * Save page to storage and to DB
     *
     * @param array $data
     * @return mixed
     */
    public function savePage(array $data)
    {
        $client = new Client();

        $response = $client->get($data['url']);
        $html = $response->getBody()->getContents();

        $parsed_html = uriToAbsolute($html, $data['url']);
        $hash = createHash();

        Storage::disk('pages')->put("$hash.txt", $parsed_html);

        return $this->model->create([
            'url' => $data['url'],
            'hash' => $hash
        ]);
    }

}