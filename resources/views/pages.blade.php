@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Ссылка на сайте</th>
                        <th>Url</th>
                        <th>Дата добавления</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($pages as $page)
                        <tr>
                            <td>
                                <a href="{{ route('page', [$page->hash]) }}">{{ route('page', [$page->hash]) }}</a>
                            </td>
                            <td>{{ $page->url }}</td>
                            <td>{{ $page->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection