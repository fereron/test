<?php

/**@var \Illuminate\Routing\Router $router */

$router->get('/', 'PagesController@showAddForm');
$router->post('/', 'PagesController@store')->name('page.save');

$router->get('/pages', 'PagesController@all')->name('pages');
$router->get('/page/{hash}', 'PagesController@show')->name('page');
